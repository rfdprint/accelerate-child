<?php
/**
 * Summary: Adds functions to the theme and child theme
 *
 * Description: This file is the child theme functions.php
 * which adds additional functions to the theme as needed.
 *
 * @link http://cosandmore.com
 *
 * @package WordPress
 *
 * @since 1.0.0 (when the file was introduced)
 */

define( 'COS_EDD_VERSION', '1.4.0' );

/**
 * -------------------------------------------------------------
 * Shortcode that adds the COS Slider DOM to the post
 *
 * @since 1.4.0
 *
 * @return string Returns the HTML tags that will trigger a DOM write for
 * the COS slider.
 * -------------------------------------------------------------
 */
function cos_slider_featured_product() {
	return '<div id="cos-featured-product-slider"></div>';
}
add_shortcode( 'cos_slider_fp', 'cos_slider_featured_product' );

/**
 * -------------------------------------------------------------
 * Description: Adds the custom post types to the blog query
 *
 * @since 1.1.0
 *
 * @param array $query Description: Contains wp query arguments.
 * -------------------------------------------------------------
 */
function fwp_home_custom_query( $query ) {
	if ( $query->is_home() && $query->is_main_query() ) {
		$query->set( 'post_type', array( 'post', 'cos-product' ) );
	}
}
add_filter( 'pre_get_posts', 'fwp_home_custom_query' );

/**
 * -------------------------------------------------------------
 * Description: Adds the custom post types to the archive type query page.
 * As when the user clicks on the archrive posts by month the post added here will
 * appear on that page.
 *
 * @since 1.1.0
 *
 * @see wp_get_archives()
 * -------------------------------------------------------------
 */
function add_post_types_to_archive( $query ) {
	$post_types = [ 'post', 'cos-product' ];
	if ( ! is_admin() && $query->is_main_query() && ! $query->is_post_type_archive( [ 'cos-project' ] ) ) {

		if ( $query->is_archive ) {
			if ( empty( $query->query_vars['suppress_filters'] ) ) {
				$query->set( 'post_type', $post_types );
				return $query;
			}
		}
	}
}
 add_filter( 'pre_get_posts', 'add_post_types_to_archive' );

 /**
  * -------------------------------------------------------------
  * Description: Adds the custom post types to the archive type query filter links.
  * As when the user clicks on the archrive posts by month link generated
  * by wp_get_archives()
  *
  * @since 1.1.0
  *
  * @see wp_get_archives()
  * -------------------------------------------------------------
  */
function ucc_getarchives_where_filter( $where, $r ) {
	$args       = array(
		'public'   => true,
		'_builtin' => false,
	);
	$output     = 'names';
	$operator   = 'and';
	$post_types = get_post_types( $args, $output, $operator );
	$post_types = array_merge( $post_types, array( 'post', 'cos-product' ) );
	$post_types = "'" . implode( "' , '", $post_types ) . "'";
	return str_replace( "post_type = 'post'", "post_type IN ( $post_types )", $where );
}
add_filter( 'getarchives_where', 'ucc_getarchives_where_filter', 10, 2 );

/**
 * -------------------------------------------------------------
 * Description: Determines is the admin tool bar is showing and if so,
 * generate styles to offset the sub menu animation.
 *
 * @since 1.0.1
 *
 * @see is_admin_bar_showing
 * -------------------------------------------------------------
 */
function cos_ajust_menu_animation() {
	$admin_menu_exsists = is_admin_bar_showing();

	if ( $admin_menu_exsists ) { ?>
<style>
@media (min-width: 530px) {
	.sub-navigation--opaque {
		top: 30px;
	}

	.scroll-trigger {
		top: -18px;
	}
}
</style>
		<?php
	}
}


/**
 * -------------------------------------------------------------
 * Summary: Allows specified html output through wp_kses
 *
 * Description: This function makes sure that only the specified HTML elements,
 * attributes, and attribute values will occur in your output through the WordPress
 * wp_kses function,and normalizes HTML entities.
 *
 * @since 1.0.0
 *
 * @see Function/method/class relied on
 * @link URL
 * @global type varname Description.
 * @global type varname Description.
 *
 * @return array Allowed html tags.
 * -------------------------------------------------------------
 */
function cos_allowed_html() {

	$allowed_tags = array(
		'a'      => array(
			'class' => array(),
			'href'  => array(),
			'rel'   => array(),
			'title' => array(),
		),
		'img'    => array(
			'alt'    => array(),
			'class'  => array(),
			'height' => array(),
			'src'    => array(),
			'width'  => array(),
		),
		'li'     => array(
			'class' => array(),
		),
		'ol'     => array(
			'class' => array(),
		),
		'p'      => array(
			'class' => array(),
		),
		'q'      => array(
			'cite'  => array(),
			'title' => array(),
		),
		'span'   => array(
			'class' => array(),
			'title' => array(),
			'style' => array(),
		),
		'strike' => array(),
		'strong' => array(),
		'ul'     => array(
			'class' => array(),
		),
	);

	return $allowed_tags;
}

/**
 * -------------------------------------------------------------
 * Summary: Returns ACF image field in the corect thumbnail size
 *
 * Description: Returns image from acf image field in the coorect
 * thumbnail size.
 *
 * @since 1.0.0
 *
 * @see get_field
 *
 * @param array  $image Description: Array contains the acf image field value.
 * @param string $thumbnail Description: String contains the thumnail size.
 * @return string Description: Returns the correct acf image size surrounded by <a> tags.
 * -------------------------------------------------------------
 */
function cos_get_acf_image( $image, $thumbnail ) {
	if ( ! empty( $image ) ) :

		// vars.
		$url     = get_the_permalink();
		$title   = $image['title'];
		$alt     = $image['alt'];
		$caption = $image['caption'];

		// thumbnail.
		$size   = $thumbnail;
		$thumb  = $image['sizes'][ $size ];
		$width  = $image['sizes'][ $size . '-width' ];
		$height = $image['sizes'][ $size . '-height' ];

		$image = '<a href="' . $url . '" title="' . $title . '"><img src="' . $thumb . '" alt="' . $alt . '" /></a>';

		return $image;

	endif;
}

/**
 * -------------------------------------------------------------
 * Description: Custom wp_query that contains class and styling parameters.
 *
 * @since 1.0.0
 *
 * @param array $args Description: Contains custom query arguments.
 * -------------------------------------------------------------.
 */
function cos_posts_meta_query( $args = null ) {
	$allowed_html = cos_allowed_html();

	if ( ! $args['section_title_color'] ) {
		$args['section_title_color'] = 'blue';
	}

	if ( ! $args['posts_per_page'] ) {
		$args['posts_per_page'] = -1;
	}

	if ( ! $args['post_type'] ) {
		$args['post_type'] = 'cos-project';
	}

	if ( ! $args['meta_key'] ) {
		$args['meta_key'] = null;
	}

	if ( ! $args['meta_value'] ) {
		$args['meta_value'] = null;
	}

	if ( ! $args['orderby'] ) {
		$args['orderby'] = 'date';
	}

	if ( ! $args['order'] ) {
		$args['order'] = 'DESC';
	}

	if ( ! $args['project'] ) {
		$project_blog_excerpt_class = null;
	} else {
		$project_blog_excerpt_class = 'blog-cos__excerpt--project';
	}

	$the_query = new WP_Query(
		array(
			'posts_per_page' => $args['posts_per_page'],
			'post_type'      => $args['post_type'],
			'meta_key'       => $args['meta_key'],
			'meta_value'     => $args['meta_value'],
			'orderby'        => $args['orderby'],
			'order'          => $args['order'],
		)
	);

	if ( $the_query->have_posts() ) :
		?>
		<?php if ( $args['meta_key'] ) { ?>
<style>
#feature-link {
	color: #5d5d5d;
	pointer-events: auto;
}

@media (min-width: 530px) {
	#feature-link {
		color: #5d5d5d;
		pointer-events: auto;

	}
}
</style>
		<?php } ?>

<h2 class="section-title section-title--<?php echo wp_kses( $args['section_title_color'], $allowed_html ); ?>">
		<?php echo wp_kses( $args['row_title'], $allowed_html ); ?>
</h2>

		<?php
		while ( $the_query->have_posts() ) :
				$the_query->the_post();
			?>
			<?php get_template_part( 'template-parts/content-' . get_post_type() ); ?>
<?php endwhile; ?>

		<?php wp_reset_postdata(); ?>
		<?php
		endif;
}

/**
 * -------------------------------------------------------------
 * Description: Limits number of words in a $arg string.
 *
 * @since 1.0.0
 *
 * @param string $content Description: $arg input value.
 * @return string Description: Returns truncated content sting.
 * -------------------------------------------------------------
 */
function cos_general_content_trim_words( $content ) {
	$length  = 10;
	$content = wp_trim_words( $content, $length );
	// $content .= '...';
	return $content;
}
add_filter( 'excerpt_length', 'cos_general_content_trim_words', 999 );

/**
 * -------------------------------------------------------------
 * Description: Limits number of words in a $arg string.
 *
 * @since 1.0.0
 *
 * @param int $length Description: $arg input max length value.
 * -------------------------------------------------------------.
 */
function cos_excerpt_length( $length ) {
	return 10;
}
add_filter( 'excerpt_length', 'cos_excerpt_length', 999 );

/**
 * -------------------------------------------------------------
 * Description: Globally set new thumbnail sizes
 *
 * @since 1.0.0
 * -------------------------------------------------------------.
 */
function cos_and_more_features() {
	add_image_size( 'pageBanner', 1024, 576, true );
	add_image_size( 'cosBlog', 340, 200, true );
	add_image_size( 'cosBlog_large', 540, 300, true ); // change for _single to large to make it more user friendly.
	add_image_size( 'cosBlogProductSlider', 740, 400, true );
	add_image_size( 'cosProject', 400, 200, true );
	add_image_size( 'cosExtraSmall', 140, 80, true );
}
add_action( 'after_setup_theme', 'cos_and_more_features' );

/**
 * -------------------------------------------------------------
 * Description: Display a hero image.
 *
 * @since 1.0.0
 *
 * @param array $args Description: Contains hero image parameters.
 * -------------------------------------------------------------.
 */
function pageBanner( $args = null ) {
	/* php header_image()*/
	if ( ! $args['title'] ) {
		$args['title'] = get_the_title();
	}
	if ( ! $args['subtitle'] ) {
		$args['subtitle'] = get_field( 'page_banner_subtitle' );
	}
	if ( ! $args['photo'] ) {
		$cos_slider_group = get_field( 'cos_slider_group' );
		if ( $cos_slider_group['cos_enable_slider'] ) {

			if ( $cos_slider_group['cos_project_slider'] ) {
				?>
<div class="project__header-slider">
				<?php
						$field          = $cos_slider_group['cos_project_slider'];
						list($sliderID) = explode( ':', $field );
						$sliderID       = trim( $sliderID );
				?>
				<?php echo do_shortcode( '[wonderplugin_slider id=' . $sliderID . ']' ); ?>
</div>
<style>
.inner-wrap {
	max-width: 100%;
}
</style>
				<?php
			}
		} else {
			if ( get_field( 'page_banner_background_image' ) ) {
				$args['photo']   = get_field( 'page_banner_background_image' );
				$page_banner_url = $args['photo']['sizes']['pageBanner']
				?>
<div class="page-banner">
	<div class="page-banner__bg-image" style="background-image: url(<?php echo esc_attr( $page_banner_url ); ?>);">
	</div>
	<div class="page-banner__text__container wrapper wrapper--padding-large t-center c-white">
		<h1 class="page-banner__title"><?php echo esc_attr( $args['title'] ); ?></h1>
				<?php if ( $args['subtitle'] ) : ?>
		<div class="page-banner__intro">
			<p><?php echo esc_attr( $args['subtitle'] ); ?></p>
		</div>
		<?php endif ?>
				<?php if ( $args['intro'] ) : ?>
		<p class="page-banner__intro--narrow-justified">
					<?php echo esc_html( $args['intro'] ); ?>
			<?php endif ?>
		</p>
	</div>
	<style>
	.inner-wrap {
		max-width: 100%;
	}

	.project__content {
		margin-top: 80px !important;
	}

	.project__table {
		margin-top: -30px;
	}
	</style>
</div>
				<?php
			}
		}
	}

}

/**
 * -------------------------------------------------------------
 * Description: Iterates the post_objects array into a echoed comma delimited list.
 *
 * @since 1.0.0
 *
 * @param array $arg Description: An array of post_objects.
 * -------------------------------------------------------------.
 */
function iterate_post_objects( $arg ) {
	$allowed_html = cos_allowed_html();

	/*
	 * Loop through post objects (assuming this is a multi-select field) ( don't setup postdata )
	 * Using this method, the $post object is never changed so all functions need a seccond parameter of the post ID in question.
	 */

	if ( $arg ) :
		$count = count( $arg );
		?>
		<?php foreach ( $arg as $i => $post_object ) : ?>

			<?php echo wp_kses( get_the_title( $post_object->ID ), $allowed_html ); ?>
			<?php
			if ( $i < $count - 1 ) {
				echo ', ';
			};
			?>
		<?php endforeach; ?>

		<?php
endif;

}

/**
 * -------------------------------------------------------------
 * Description: Iterates table columns to an array.
 *
 * @since 1.0.0
 *
 * @param string $arg_table Description: Contains table name.
 * @return string Description: Returns an array of database column names and ids.
 * -------------------------------------------------------------.
 */
function addon_database_query_list( $arg_table ) {
	$table_name = '';
	if ( 'gridgallery' === $arg_table ) {

		$table_name = 'wp_wonderplugin_gridgallery';
	} else {
		$table_name = 'wp_wonderplugin_slider';
	}

	global $wpdb;
	$gallery = '';
	$data    = [];

	$qry    = "SELECT * FROM $table_name";
	$result = $wpdb->get_results( $qry );

	foreach ( $result as $key => $row ) {
		// each column in your row will be accessible like this.
		$arg_list = array(
			'label' => $row->name,
			'id'    => $row->id,
		);
		array_push( $data, $arg_list );
	}
	return $data;
}

/**
 * -------------------------------------------------------------
 * Description: Iterates the terms array into a echoed comma delimited list.
 *
 * @since 1.0.0
 *
 * @param array $terms Description: An array of terms.
 * -------------------------------------------------------------.
 */
function iterate_terms( $terms ) {
	$allowed_html = cos_allowed_html();
	if ( $terms ) {
		$count = count( $terms );
		echo '<p>';
		foreach ( $terms as $i => $term ) {

			echo wp_kses( $term->name, $allowed_html );
			if ( $i < $count - 1 ) {
				echo ', ';
			};
		};
		echo '</p>';
	}
}

/**
 * -------------------------------------------------------------
 * Summary: Loads grid gallery names and ids to the corresponding ACF field.
 *
 * Description: Loads wonder grid gallery names and ids to the
 * corresponding ACF field on the project edit page.
 *
 * @since 1.0.0
 *
 * @param array $field  Description: Used by ACF. It is ACF slect field choices
 * -------------------------------------------------------------.
 */
function acf_load_grid_gallery_field_choices( $field ) {
	// reset choices.
	$field['choices'] = array();

	$choices = addon_database_query_list( 'gridgallery' );

	// loop through array and add to field 'choices'.
	if ( is_array( $choices ) ) {

		foreach ( $choices as $choice ) {
			$value                                       = $choice['id'];
			$label                                       = $choice['label'];
			$field['choices'][ $value . ' : ' . $label ] = $label;
		}
	}

	return $field;

}

add_filter( 'acf/load_field/name=cos_project_gallery', 'acf_load_grid_gallery_field_choices' );

/**
 * -------------------------------------------------------------
 * Summary: Loads slider names and ids to the corresponding ACF field.
 *
 * Description: Loads wonder slider names and ids to the
 * corresponding ACF field on the project edit page.
 *
 * @since 1.0.0
 *
 * @param array $field  Description: Used by ACF. It is ACF slect field choices
 * -------------------------------------------------------------.
 */
function acf_load_slider_field_choices( $field ) {
	// reset choices.
	$field['choices'] = array();

	$choices = addon_database_query_list( 'slider' );

	// loop through array and add to field 'choices'.
	if ( is_array( $choices ) ) {

		foreach ( $choices as $choice ) {
													$value = $choice['id'];
													$label = $choice['label'];
			$field['choices'][ $value . ' : ' . $label ]   = $label;
		}
	}

	// return the field.
	return $field;

}
add_filter( 'acf/load_field/name=cos_project_slider', 'acf_load_slider_field_choices' );

/**
 * -------------------------------------------------------------
 * Description: Loads a custom CTA box above the footer.
 *
 * @see accelerate_before_footer
 *
 * @since 1.0.0
 * -------------------------------------------------------------
 */
function cos_cta_get_in_touch() {
	$contactPage    = get_page_by_title( 'contact' );
	$contactPageURL = get_permalink( $contactPage );
	echo '
 <div class="cta__git">
			<div class="cta__git-header">Get in Touch</div>
			<div class="cta__git-text">What can we do for you?</div>
			<div class="cta__btn-wrapper"><a href="' . esc_url( $contactPageURL ) . '" class="btn--cta-git">Contact Us</a></div>
		</div>';

}
add_action( 'accelerate_before_footer', 'cos_cta_get_in_touch' );

/**
 * -------------------------------------------------------------
 * Description: Loads child theme styles and scripts
 *
 * @since 1.0.0
 * -------------------------------------------------------------
 */
function cos_and_more_files() {
	$parent_style = 'parent-style'; // This is 'twentyfifteen-style' for the Twenty Fifteen theme.

	wp_enqueue_style(
		$parent_style,
		get_template_directory_uri() . '/style.css',
		null,
		wp_get_theme()->get( 'Version' )
	);

	wp_enqueue_style(
		'child-style',
		get_stylesheet_directory_uri() . '/style.css',
		array( $parent_style ),
		wp_get_theme()->get( 'Version' )
	);

	// wp_enqueue_style('font-awesome', '//maxcdn.bootstrapcdn.com/font-awesome/5.4.0/css/font-awesome.min.css');

	wp_enqueue_script(
		'cos-app-js', get_stylesheet_directory_uri()
		. '/js/bundled/App-Bundled.js', null, COS_EDD_VERSION, true
	);
	wp_localize_script(
		'cos-app-js',
		'cosData',
		array(
			'root_url' => get_site_url(),
			'nonce'    => wp_create_nonce( 'wp_rest' ),
		)
	);

	wp_enqueue_script(
		'cos-vendor-js', get_stylesheet_directory_uri()
		. '/js/bundled/Vendor-Bundled.js', null, COS_EDD_VERSION, true
	);

	wp_enqueue_script(
		'cos-react-js', get_stylesheet_directory_uri()
		. '/js/bundled/React-Bundled.js', null, COS_EDD_VERSION, true
	);

}
add_action( 'wp_enqueue_scripts', 'cos_and_more_files' );


require 'post-types.php';
require 'taxonomies.php';
require 'custom-fields.php';
require get_theme_file_path( '/inc/featured-products-route.php' );
require get_theme_file_path( '/inc/search-route.php' );
?>
