<?php
/**
 * Theme Page Section for our theme.
 *
 * @package ThemeGrill
 * @subpackage Accelerate
 * @since Accelerate 1.0
 */
get_header(); ?>

<?php do_action( 'accelerate_before_body_content' ); ?>

<style>
li.current-menu-item a {
	font-weight: 700;
}
</style>
<div>
	<div class="cos-content">

	<div class="project__anchor"  id="feature-projects"></div>
		<div class="page-section" data-matching-link="#feature-link">
			<div class="wrapper">

				<div class="row row--equal-height-at-large row--gutters-small">
					<?php

					$featuredPost = cos_posts_meta_query(
						array(
							'post_type'      => 'cos-project',
							'row_class'      => 'row--post-center',
							'row_title'      => 'Featured Projects',
							'posts_per_page' => 3,
							'meta_key'       => 'cos_featured_post',
							'meta_value'     => true,
						)
					);
					?>
				</div>
			</div>
		</div>
<!--
		<div class="project__anchor" id="recent-projects"></div>
		<div class="page-section page-section--grey" data-matching-link="#recent-link">
			<div class="wrapper">
				<div class="row row--equal-height-at-large row--gutters-small">

					<?php /*
					$recentPost = cos_posts_meta_query(
						array(
							'post_type'           => 'cos-project',
							'row_title'           => 'Recent Projects',
							'thumb'               => 'cosBlog',
							'posts_per_page'      => 3,
							'section_title_color' => 'white',
						)
					);


					*/?>

				</div>
			</div>
		</div>
-->
		<div class="project__anchor" id="all-projects"></div>
		<div class="page-section page-section--grey" data-matching-link="#all-link">
			<div class="wrapper">
				<div class="row row--equal-height-at-large row--gutters-small">
					<?php
					$allPost = cos_posts_meta_query(
						array(
							'post_type' => 'cos-project',
							'project'   => true,
							'row_title' => 'Project Archive',
							'row_parm'  => 'large-4',
							'thumb'     => 'cosBlog',
							'section_title_color' => 'white',
							'orderby' => 'title',
							'order'   => 'ASC',
						)
					);

					?>
				</div>
			</div>
		</div>

	</div><!-- #content -->
</div><!-- #primary -->


<?php // accelerate_sidebar_select('cos-project'); ?>

<?php do_action( 'accelerate_after_body_content' ); ?>

<?php get_footer(); ?>
