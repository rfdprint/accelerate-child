<?php
/**
 * WP Rest Route cos/v1/featured-products
 *
 * Returns WP Rest Route cos/v1/featured-products WP_Query results. The WP_Query finds the cos-product post types
 * post, where meta key cos_featured_product is true, are returned in the json results.
 *
 * @package WordPress
 *
 * @since 1.3.0 (when the file was introduced)
 */

/**
 * -------------------------------------------------------------
 * Registers WP REST route cos/v1/featured-products.
 *
 * @since 1.3.0
 *
 * -------------------------------------------------------------
 */
function cosRegisterProductsRoute() {
	register_rest_route(
		'cos/v1', 'featured-products', array(
			'methods'  => WP_REST_SERVER::READABLE,
			'callback' => 'cosProductsRouteResults',
		)
	);
}
add_action( 'rest_api_init', 'cosRegisterProductsRoute' );

/**
 * -------------------------------------------------------------
 * Returns the WP Rest Route cos/v1/featured-products WP_Query results.
 * The WP_Query finds the cos-product post types post, where meta key
 * cos_featured_product is true, are returned in the json results.
 *
 * @since 1.3.0
 *
 * @return json Returns search details from WP_Query.
 * -------------------------------------------------------------
 */
function cosProductsRouteResults() {

	$mainQuery = new WP_Query(
		array(
			'post_type'  => array( 'cos-product' ),
			'meta_key'   => 'cos_featured_product',
			'meta_value' => true,
			'posts_per_page' => 10
		)
	);

	/*$results = array(
		'featuredProducts' => array(),
	);*/
	$results = [];
	while ( $mainQuery->have_posts() ) {
		$mainQuery->the_post();

		$description = null;
		$featured    = false;

		if ( function_exists( 'get_field' ) ) {
			get_field( 'cos_featured_product' );
			$featured = get_field( 'cos_featured_product' );
		}

		if ( function_exists( 'get_field' ) ) {
			get_field( 'cos_product_description' );
			$description = get_field( 'cos_product_description' );
		}

		$imageUrl = null;
		if ( function_exists( 'get_field' ) ) {
			get_field( 'cos_project_description' );
			$description = get_field( 'cos_product_description' );
		}


			array_push(
				$results, array(
					'title'                         => get_the_title(),
					'permalink'                     => get_the_permalink(),
					'description'                   => $description,
					'featured'                      => $featured,
					'post_type'                     => get_post_type(),
					'imageCosBlogUrl'               => get_the_post_thumbnail_url( 0, 'cosBlog' ),
					'imageCosBlogLargeUrl'          => get_the_post_thumbnail_url( 0, 'cosBlog_large' ),
					'imageCosBlogProductSliderUrl'  => get_the_post_thumbnail_url( 0, 'cosBlogProductSlider' ),
					'sliderThumbnailExtraSmallUrl' 	=> get_the_post_thumbnail_url( 0, 'cosExtraSmall' ),
					'authorName'                    => get_the_author(),
					'postDate'                      => get_the_date(),
					'postId'                        => get_the_ID(),
				)
			);

	}

	return $results;
}
