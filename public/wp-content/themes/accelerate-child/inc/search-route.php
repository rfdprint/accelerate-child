<?php
/**
 * WP Rest Route cos/v1/search
 *
 * Returns WP Rest Route cos/v1/search WP_Query results. The individual post types
 * are seperated into easily identifiable arrays() which are returned in the json results.
 *
 * @package WordPress
 *
 * @since 1.3.0 (when the file was introduced)
 */

/**
 * -------------------------------------------------------------
 * Registers WP REST route cos/v1/search.
 *
 * @since 1.3.0
 *
 * -------------------------------------------------------------
 */
function cosRegisterSearch() {
	register_rest_route(
		'cos/v1', 'search', array(
			'methods'  => WP_REST_SERVER::READABLE,
			'callback' => 'cosSearchResults',
		)
	);
}

add_action( 'rest_api_init', 'cosRegisterSearch' );

/**
 * -------------------------------------------------------------
 * Returns the WP Rest Route cos/v1/search WP_Query results. The individual post types
 * are seperated into easily identifiable arrays() which are returned in the json results.
 *
 * @since 1.3.0
 *
 * @param string $data Sanitized search url parameter.
 *
 * @return json Returns search details from WP_Query.
 * -------------------------------------------------------------
 */
function cosSearchResults( $data ) {
	$mainQuery = new WP_Query(
		array(
			'post_type' => array( 'post', 'page', 'cos-project', 'cos-product' ),
			's'         => sanitize_text_field( $data['term'] ),
		)
	);

	$results = array(
		'generalInfo' => array(),
		'cos-project' => array(),
		'cos-product' => array(),
	);

	while ( $mainQuery->have_posts() ) {
		$mainQuery->the_post();

		// Page and Post details.
		if ( get_post_type() === 'page' || get_post_type() === 'post' ) {
			array_push(
				$results['generalInfo'], array(
					'title'      => get_the_title(),
					'permalink'  => get_the_permalink(),
					'post_type'  => get_post_type(),
					'authorName' => get_the_author(),
				)
			);
		}

		// Project details.
		if ( get_post_type() === 'cos-project' ) {
			$description = null;

			if ( function_exists( 'get_field' ) ) {
				get_field( 'cos_project_description' );
				$description = get_field( 'cos_project_description' );
			}

			if ( function_exists( 'get_field' ) ) {
				$image = get_field( 'page_banner_background_image' );
				$size  = 'cosBlog'; // (thumbnail, medium, large, full or custom size).
				if ( $image ) {
					$imageUrl = $image['url'];
				}
			}

			array_push(
				$results['cos-project'], array(
					'title'       => get_the_title(),
					'permalink'   => get_the_permalink(),
					'image'       => $imageUrl,
					'description' => $description,
				)
			);
		}

		 // Product details.
		if ( get_post_type() === 'cos-product' ) {
			$description = null;
			$imageUrl    = null;
			$featured    = false;

			if ( function_exists( 'get_field' ) ) {
				get_field( 'cos_featured_product' );
				$featured = get_field( 'cos_featured_product' );
			}
			if ( function_exists( 'get_field' ) ) {
				get_field( 'cos_project_description' );
				$description = get_field( 'cos_product_description' );
			}

			array_push(
				$results['cos-product'], array(
					'title'       => get_the_title(),
					'permalink'   => get_the_permalink(),
					'description' => $description,
					'featured'    => $featured,
					'post_type'   => get_post_type(),
					'image'       => get_the_post_thumbnail_url( 0, 'cosBlog' ),
					'authorName'  => get_the_author(),
					'postDate'    => get_the_date(),
				)
			);
		}
	}

	return $results;
}
