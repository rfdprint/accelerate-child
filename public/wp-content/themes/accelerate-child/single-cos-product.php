<?php
/**
 * Template for COS Projects
 */
get_header(); ?>
<?php
	pageBanner();
?>
<div class="inner-wrap-restore">

	<?php do_action( 'accelerate_before_body_content' ); ?>
	<h2 class="blog-cos__title blog-cos__title--single ">
	<?php  the_title() ?>
	<hr>
</h2>

	<div class="product__content content__w-bulleted-list" id="content"  class="clearfix">
	<?php
			if ( has_post_thumbnail() ) {
				$image           = '';
				$title_attribute = get_the_title( $post->ID );
				$image          .= '<figure class="blog__image">';
				$image          .= '<a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '">';
				$image          .= get_the_post_thumbnail(
					$post->ID, 'cosBlog_large', array(
						'title' => esc_attr( $title_attribute ),
						'alt'   => esc_attr( $title_attribute ),
					)
				) . '</a>';
				$image          .= '</figure>';
				echo $image;
			}
			?>
		<div >
			<?php
			while ( have_posts() ) :
				the_post();
				?>

			<div>
			<?php
				if (function_exists('get_field')) :
					$content = get_field( 'cos_product_description' );
					if ($content) : ?>

								<?php echo $content;?>
					<?php endif ?>
				<?php endif ?>

			</div>

			<div>
			<?php
				if (function_exists('get_field')) :
					$additionalContent = get_field( 'cos_product_additional_content' );
					if ($additionalContent) : ?>
					<hr>

								<?php echo $additionalContent;?>
					<?php endif ?>
				<?php endif ?>

			</div>
			<?php endwhile; ?>


		</div><!-- #content -->
	</div><!-- #primary -->

	<?php do_action( 'accelerate_after_body_content' ); ?>

<?php get_footer(); ?>
