<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package ThemeGrill
 * @subpackage Accelerate
 * @since Accelerate 1.0
 */
?>


<?php do_action( 'accelerate_before_post_content' ); ?>
<h2 class="blog-cos__title blog-cos__title--single ">
	<?php  the_title() ?>
	<hr>
</h2>

<div class="entry-content clearfix">
	<?php
			if( has_post_thumbnail() ) {
				$image = '';
	     		$title_attribute = get_the_title( $post->ID );
	     		$image .= '<figure class="post-featured-image blog-cos__img--large">';
	  			$image .= '<a  href="' . get_permalink() . '" title="'.the_title_attribute( 'echo=0' ).'">';
	  			$image .= get_the_post_thumbnail( $post->ID, 'cosBlog_large', array( 'title' => esc_attr( $title_attribute ), 'alt' => esc_attr( $title_attribute ) ) ).'</a>';
	  			$image .= '</figure>';
	  			echo $image;
	  		}
		?>


	<?php
			the_content();

			wp_link_pages( array(
				'before'            => '<div style="clear: both;"></div><div class="pagination clearfix">'.__( 'Pages:', 'accelerate' ),
				'after'             => '</div>',
				'link_before'       => '<span>',
				'link_after'        => '</span>'
	      ) );
		?>
</div>

<?php do_action( 'accelerate_after_post_content' ); ?>