import React from "react";
import ReactDOM from "react-dom";
import FeaturedProductsSlider from "./react-modules/FeaturedProductsSlider";

export const App = () => {
	return <FeaturedProductsSlider />;
};

const sliderDom = document.getElementById("cos-featured-product-slider");

if (sliderDom) {
	ReactDOM.render(<App />, sliderDom);
}
