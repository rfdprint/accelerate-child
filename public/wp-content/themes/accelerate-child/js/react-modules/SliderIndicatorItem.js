import React from "react";
import { useState } from "react";

let styles = { display: "none" };

/*const clearShowThumbanail = () => {
	console.log("clear");
	let thumb;
	if ((thumb = document.querySelector(".slider-indicator-thumbnail"))) {
		let style = window.getComputedStyle
			? getComputedStyle(thumb, null)
			: thumb.currentStyle;
		thumb.style.display = "none";
		//console.log(thumb);
	}
};*/

const SliderIndicatorItem = ({
	index,
	currentIndex,
	handleClick,
	indicatorThumbnails
}) => {
	const [thumbnailVisible, setThumbnailVisible] = useState();
	const [translateValue, setTranslateValue] = useState(0);
	const showThumbanail = index => {
		setThumbnailVisible(true);
		setTranslateValue(index + -55);
	};

	const hideThumbanail = () => {
		setThumbnailVisible(false);
	};

	if (index === currentIndex) {
		return (
			<div
				onMouseEnter={() => {
					showThumbanail(index);
				}}
				onMouseLeave={() => {
					hideThumbanail();
				}}>
				<div
					className={`slider-thumbnail-wrapper ${
						thumbnailVisible
							? "slider-thumbnail-image-show"
							: "slider-thumbnail-image-hide"
					}`}
					style={{
						transform: `translateX(${translateValue}px)`
					}}>
					<img src={indicatorThumbnails[index]} />
				</div>

				<div className="slider-indicator-item slider-indicator-item-current">
					<i className="fa fa-circle fa-xs" aria-hidden="true"></i>
				</div>
			</div>
		);
	} else {
		return (
			<div
				onClick={handleClick(index)}
				onMouseEnter={() => {
					showThumbanail(index);
				}}
				onMouseLeave={() => {
					hideThumbanail(index);
				}}>
				<div
					className={`slider-thumbnail-wrapper ${
						thumbnailVisible
							? "slider-thumbnail-image-show"
							: "slider-thumbnail-image-hide"
					}`}
					style={{
						transform: `translateX(${translateValue}px)`
					}}>
					<img
						className="slider-indicator-item-thumbnail"
						src={indicatorThumbnails[index]}
					/>
				</div>

				<div className="slider-indicator-item">
					<i className="fa fa-circle fa-xs" aria-hidden="true"></i>
					{/*fa fa-circle-o"*/}
				</div>
			</div>
		);
	}
};

export default SliderIndicatorItem;
