//Packages
import React from "react";
import { useState, useEffect } from "react";
import axios from "axios";
//Components
import Slide from "./Slide";
import RightArrow from "./RightArrow";
import LeftArrow from "./LeftArrow";
import SliderIndicator from "./SliderIndicator";

//Defualt React Component
const FeaturedProductsSlider = () => {
	const [images, setImages] = useState();
	const [currentIndex, setCurrentIndex] = useState(0);
	const [translateValue, setTranslateValue] = useState(0);
	const [elementAnimation, setElementAnimation] = useState(null);
	const [buttonAnimation, setButtonAnimation] = useState(null);

	//Get responsive slide width to calculate translation
	const slideWidth = () => {
		let slide;
		if ((slide = document.querySelector(".slider-slide"))) {
			return slide.scrollWidth;
		}
	};

	const handleIndicatorClick = indicatorIndex => {
		let width = slideWidth();
		let slideLocation = indicatorIndex * -width;

		setCurrentIndex(indicatorIndex);
		setTranslateValue(slideLocation);
		handleElementAnimation();
		handleButtonAnimation();
	};

	const handleElementAnimation = () => {
		setElementAnimation("fadeElementOut");
		setTimeout(function() {
			setElementAnimation("floatElementUp");
		}, 500);
	};

	const handleButtonAnimation = () => {
		setButtonAnimation("fadeElementOut");
		setTimeout(function() {
			setButtonAnimation("floatButtonUp");
		}, 1000);
	};

	useEffect(() => {
		console.log(cosData.root_url);
		axios
			.get(cosData.root_url + "/wp-json/cos/v1/featured-products")
			.then(res => {
				let data = res.data;
				setImages({
					...images,
					...data
				});
				handleElementAnimation();
				handleButtonAnimation();
			})
			.catch(error => {
				console.log(error);
			});
	}, []);

	const goToPrevSlide = () => {
		if (currentIndex === 0) return;
		handleElementAnimation();
		handleButtonAnimation();
		setCurrentIndex(currentIndex - 1);
		setTranslateValue(translateValue + slideWidth());
	};

	const goToNextSlide = () => {
		handleElementAnimation();
		handleButtonAnimation();
		let numberOfImages = Object.entries(images).length;
		if (currentIndex === numberOfImages - 1) {
			setCurrentIndex(0);
			setTranslateValue(0);
			return;
		}
		setCurrentIndex(currentIndex + 1);
		setTranslateValue(translateValue + -slideWidth());
	};

	if (images) {
		let numberOfImages = Object.entries(images).length;
		if (numberOfImages > 0) {
			let indicatorThumbnails = Object.entries(images).map(image => {
				image = image[1];
				return image.sliderThumbnailExtraSmallUrl;
			});
			return (
				<div>
					<div className="slider">
						<h2 className="slider-main-heading">
							Featured Products
						</h2>
						<div
							className="slider-wrapper"
							style={{
								transform: `translateX(${translateValue}px)`,
								transition: "transform ease-out 0.45s"
							}}>
							{Object.entries(images).map(image => {
								image = image[1];

								return (
									<span key={image.postId}>
										<Slide
											postId={image.postId}
											image={
												image.imageCosBlogProductSliderUrl
											}
											title={image.title}
											elementAnimation={elementAnimation}
											buttonAnimation={buttonAnimation}
											postLink={image.permalink}
											thumbnail={
												image.sliderThumbnailExtraSmallUrl
											}
										/>
									</span>
								);
							})}
						</div>
						<LeftArrow goToPrevSlide={goToPrevSlide} />
						<RightArrow goToNextSlide={goToNextSlide} />
					</div>
					<SliderIndicator
						numberOfImages={numberOfImages}
						currentIndex={currentIndex}
						handleClick={indicatorIndex =>
							handleIndicatorClick.bind(this, indicatorIndex)
						}
						indicatorThumbnails={indicatorThumbnails}
					/>
				</div>
			);
		} else {
			return (
				<div className="slider-no-images-found">
					There are no featured products currently, but check back
					again soon.
				</div>
			);
		}
	} else {
		return (
			<div className="slider-no-images-found">
				There are no featured products currently, but check back again
				soon.
			</div>
		);
	}
};

export default FeaturedProductsSlider;
