import React from "react";

const SliderImageTitle = ({ title, elementAnimation }) => {
	let sliderCLass = `slider-title ${elementAnimation}`;

	return (
		<div className={sliderCLass}>
			<div className="slider-title-background">
				<div className="slider-title-text">{title}</div>
			</div>
		</div>
	);
};

export default SliderImageTitle;
