import React from "react";
import SliderImageTitle from "./SliderImageTitle";
import SliderButton from "./SliderButton";

const Slide = ({
	image,
	title,
	elementAnimation,
	buttonAnimation,
	postLink
}) => {
	const styles = {
		backgroundImage: `url(${image})`,
		backgroundSize: "cover",
		backgroundRepeat: "no-repeat",
		backgroundPosition: "50% 60%",
		position: "relative"
	};
	return (
		<div className="slider-slide" style={styles}>
			<SliderImageTitle
				title={title}
				elementAnimation={elementAnimation}
			/>
			<SliderButton
				title={title}
				elementAnimation={elementAnimation}
				buttonAnimation={buttonAnimation}
				postLink={postLink}
			/>
		</div>
	);
};

export default Slide;
