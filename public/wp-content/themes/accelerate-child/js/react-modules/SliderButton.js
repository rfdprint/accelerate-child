import React from "react";

const SliderButton = ({ buttonAnimation, postLink }) => {
	let sliderCLass = `btn slider-button ${buttonAnimation}`;
	return (
		<a href={postLink} className={sliderCLass}>
			Learn More
		</a>
	);
};
export default SliderButton;
