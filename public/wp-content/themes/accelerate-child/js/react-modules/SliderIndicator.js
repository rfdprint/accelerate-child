import React from "react";

import SliderIndicatorItem from "./SliderIndicatorItem";

const SliderIndicator = ({
	numberOfImages,
	currentIndex,
	handleClick,
	indicatorThumbnails
}) => {
	return (
		<div className="slider-indicator">
			{Array.from({ length: numberOfImages }, (value, index) => {
				return (
					<SliderIndicatorItem
						key={index}
						index={index}
						currentIndex={currentIndex}
						handleClick={handleClick}
						indicatorThumbnails={indicatorThumbnails}
					/>
				);
			})}
		</div>
	);
};

export default SliderIndicator;
