<?php
function cos_post_types() {

	register_post_type(
		'cos-project',
		array(
			'show_in_rest' => true,
			'supports'     => array( 'title', 'excerpt' ),
			'public'       => true,
			'has_archive'  => true,
			'taxonomies'   => array( 'location', 'market', 'product-category' ),
			'rewrite'      => array( 'slug' => 'projects' ),
			'labels'       => array(
				'name'                      => 'COS Project',
				'all_items'                 => esc_html__( 'All Project', 'text-domain' ),
				'view_item'                 => esc_html__( 'View Project', 'text-domain' ),
				'view_items'                => esc_html__( 'View Projects', 'text-domain' ),
				'search_items'              => esc_html__( 'Search Projects', 'text-domain' ),
				'add_new_item'              => esc_html__( 'Add New Project', 'text-domain' ),
				'add_new'                   => esc_html__( 'Add New', 'text-domain' ),
				'edit_item'                 => esc_html__( 'Edit Project', 'text-domain' ),
				'singular_name'             => esc_html__( 'Project', 'text-domain' ),
				'uploaded_to_this_item'     => esc_html__( '‘Uploaded to this project', 'text-domain' ),
				'set_featured_image'        => esc_html__( 'Set project image', 'text-domain' ),
				'remove_featured_image'     => esc_html__( 'Remove project image ', 'text-domain' ),
				'use_featured_image'        => esc_html__( 'Use project image', 'text-domain' ),
				'item_published_privately'  => esc_html__( 'Project published privately', 'text-domain' ),
				'item_reverted_to_draft'    => esc_html__( 'Project reverted to draft', 'text-domain' ),
				'item_scheduled'            => esc_html__( 'Project scheduled', 'text-domain' ),
				'item_updated'              => esc_html__( 'Project updated', 'text-domain' ),
			),
			'menu_icon'    => 'dashicons-clipboard',
		)
	);

	register_post_type(
		'cos-product',
		array(
			'show_in_rest' => true,
			'supports'     => array( 'title', 'thumbnail' ),
			'public'       => true,
			'has_archive'  => true,
			'taxonomies'   => array( 'market', 'product-category' ),
			'rewrite'      => array( 'slug' => 'products' ),
			'labels'       => array(
				'name'                      => 'COS Product',
				'all_items'                 => esc_html__( 'All Products', 'text-domain' ),
				'view_item'                 => esc_html__( 'View Product', 'text-domain' ),
				'view_items'                => esc_html__( 'View Products', 'text-domain' ),
				'add_new_item'              => esc_html__( 'Add New Product', 'text-domain' ),
				'search_items'              => esc_html__( 'Search Product', 'text-domain' ),
				'add_new'                   => esc_html__( 'Add New', 'text-domain' ),
				'edit_item'                 => esc_html__( 'Edit Product', 'text-domain' ),
				'singular_name'             => esc_html__( 'Product', 'text-domain' ),
				'uploaded_to_this_item'     => esc_html__( '‘Uploaded to this product', 'text-domain' ),
				'set_featured_image'        => esc_html__( 'Set product image ', 'text-domain' ),
				'remove_featured_image'     => esc_html__( 'Remove product image ', 'text-domain' ),
				'featured_image'            => esc_html__( 'Product Image ', 'text-domain' ),
				'use_featured_image'        => esc_html__( 'Use product image ', 'text-domain' ),
				'item_published_privately'  => esc_html__( 'Product published privately', 'text-domain' ),
				'item_reverted_to_draft'    => esc_html__( 'Product reverted to draft', 'text-domain' ),
				'item_scheduled'            => esc_html__( 'Product scheduled', 'text-domain' ),
				'item_updated'              => esc_html__( 'Product updated', 'text-domain' ),
			),
			'menu_icon'    => 'dashicons-clipboard',
		)
	);
}
add_action( 'init', 'cos_post_types' );

add_filter( 'enter_title_here', 'my_title_place_holder', 20, 2 );
function my_title_place_holder( $title, $post ) {

	if ( $post->post_type == 'cos-product' ) {
		$my_title = 'Add product name';
		return $my_title;
	}

	return $title;

}

add_action( 'do_meta_boxes', 'wpse33063_remove_meta_box' );

function wpse33063_remove_meta_box() {
	remove_meta_box( 'marketdiv', 'cos-project', 'side' );
	remove_meta_box( 'tagsdiv-product-category', 'cos-project', 'side' );
	remove_meta_box( 'tagsdiv-location', 'cos-project', 'side' );
	remove_meta_box( 'tagsdiv-manufacturer', 'cos-project', 'side' );
}

