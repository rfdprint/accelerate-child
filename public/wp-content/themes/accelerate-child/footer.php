<?php
/**
 * Theme Footer Section for our theme.
 *
 * Displays all of the footer section and closing of the #main div.
 *
 * @package ThemeGrill
 * @subpackage Accelerate
 * @since Accelerate 1.0
 */
?>

</div><!-- .inner-wrap -->
</div><!-- #main -->
<?php do_action( 'accelerate_before_footer' ); ?>
<footer class="site-footer" id="colophon" class="clearfix">
	<?php get_sidebar( 'footer' ); ?>

	<div class="site-footer__inner wrapper">
		<div class="group row row--gutters row--gutters-small">
			<div class="site-footer__col-one row__medium-6">
				<h2 class="headline--footer">Connect With Us</h2>
				<div class="site-footer__content--justify">

					<div class="social-icons">
						<!--<a href="#" class="social-icons__icon"><span class="icon icon--instagram"></span></a>-->
						<a href="https://www.facebook.com/Creative-Office-Solutions-more-125913362141884/"
							class="social-icons__icon"><span class="icon icon--facebook"></span></a>
						<a href="https://www.linkedin.com/company/creative-office-solutions-miami/about/"
							class="social-icons__icon"><span class="icon icon--linkedin"></span></a>
					</div>

				</div>
				<hr />
				<div class="site-footer__copyright">
					<p>Content By COS & More | Website Theme Modifications By RFDPrint</p>
					<p>© Copyright 2019 COS & More, LLC.</p>
				</div>
			</div>

			<div class="site-footer__col-two row__medium-6">
				<h2 class="headline--footer">Subscribe to Our Newsletter</h2>
				<div class="site-footer__content--narrow">
					<p>Your personal information will be kept private and will not be shared with others.
						See our <span><a class="link-inverted"
								href="<?php echo esc_url( site_url( '/privacy-policy' ) ); ?>">Privacy Policy</a></span>
					</p>
				</div>
				<div class="form form__footer">
					<?php echo do_shortcode( '[contact-form-7 id="745" title="Footer Newsletter"]' ); ?>

					<!--<input placeholder="Enter Your Email Address" id="footer-email" class="form__footer-email" type="text">
					<button name="footer-submit" id="footer-submit" class="btn--large">Submit</button><br>
					<label class="form__info__footer"></label>-->
				</div>
			</div>
			<span id="my_email_ajax_nonce" data-nonce="<?php echo wp_create_nonce( 'my_email_ajax_nonce' ); ?>"></span>

		</div>
</footer>
<a href="#masthead" id="scroll-up"><i class="fa fa-long-arrow-up"></i></a>
</div><!-- #page -->
<?php wp_footer(); ?>
</body>

</html>