<?php
/**
 * Template for COS Projects
 */
get_header(); ?>
<?php
	pageBanner();
?>
<div class="inner-wrap-restore">

	<?php do_action( 'accelerate_before_body_content' ); ?>

	<div class="project__content content__w-bulleted-list">
		<div id="content"  class="clearfix">
			<?php
			while ( have_posts() ) :
				the_post();
				?>

<table class="project__table">
<tr>
    <th colspan="2" class="project__table__title"><?php the_title() ?></th>
  </tr>
  <tr>
    <th class="project__table__header">Location:</th>
	<td>
	<?php
		$locations = get_field('cos_locations');
		iterate_terms($locations);
	?>
	</td>
  </tr>
  <tr>
    <th class="project__table__header">Markets:</th>
    <td>
	<?php
		$markets = get_field('cos_markets');
		iterate_terms($markets);
	?>
	</td>
  </tr>
  <tr>
    <th class="project__table__header">Product Solutions:</th>
    <td>
	<?php
		$markets = get_field('cos_product_categories');
		iterate_terms($markets);
	?>
	</td>
  </tr>
  <tr>
    <th class="project__table__header">Manufacturers:</th>
    <td>

	<?php
		$manufacturers = get_field('cos_manufacturers');
		iterate_terms($manufacturers);

	?>
	</td>
  </tr>
	<?php
	if (function_exists('get_field')) :
		$content = get_field( 'cos_project_description' );
		if ($content) : ?>

			<td colspan="2">
				<div class="project__table__header">Description:</div>
				<div>
					<?php echo $content;?>
				</div>
			</td>

		<?php endif ?>
	<?php endif ?>
</table>
			<?php endwhile; ?>

			<?php echo '<div class="project__gallery">'; ?>
			<?php
				$field = get_field('cos_project_gallery');
				$galleryID = $field['value'];

			?>
				<?php echo do_shortcode( '[wonderplugin_gridgallery id='.$galleryID.']' ); ?>
			<?php echo '</div>'; ?>
		</div><!-- #content -->
	</div><!-- #primary -->

	<div> <!-- #inner-wrap-restore -->
	<?php do_action( 'accelerate_after_body_content' ); ?>

<?php get_footer(); ?>
