<?php
/**
 * The template used for displaying blog small image post.
 *
 * @package ThemeGrill
 * @subpackage Accelerate
 * @since Accelerate 1.0
 */
?>

<div class="row__medium-6">
	<div class="blog-cos wrapper--shadow">
		<?php	$post_type = get_post_type();
					if ($post_type == 'cos-product'){?>
		<div class="blog-cos__featured-product-header">Featured Product</div>
		<?php } ?>
		<?php
			if ( has_post_thumbnail() ) {
				$image           = '';
				$title_attribute = get_the_title( $post->ID );
				$image          .= '<figure class="blog__image">';
				$image          .= '<a href="' . get_permalink() . '" title="' . the_title_attribute( 'echo=0' ) . '">';
				$image          .= get_the_post_thumbnail(
					$post->ID, 'cosBlog', array(
						'title' => esc_attr( $title_attribute ),
						'alt'   => esc_attr( $title_attribute ),
					)
				) . '</a>';
				$image          .= '</figure>';
				echo $image;
			}
			?>
		<div class="headline--small blog-cos__title">
			<a href="<?php the_permalink()?>"> <?php the_title(); ?></a>
		</div>

		<div class="blog-cos__excerpt ">
			<?php
					$post_type = get_post_type();
					if ($post_type == 'post'){
					global $more;
					$more = 0;
					the_excerpt( '<span>' . __( 'Read more', 'accelerate' ) . '</span>' );
					} elseif ( $post_type == 'cos-product'){
						if (function_exists('get_field')) {
						 $productDescription = get_field( 'cos_product_description' );
						 $productExcerpt = cos_general_content_trim_words(  $productDescription );
						 echo '<p>' . $productExcerpt . '</p>';
						}
					}
				?>

		</div>

		<div class="blog-cos__wrapper-lower">

			<div class="blog-cos_meta">
				<i class="fa fa-calendar-o"></i>
				<span><?php $post_date = get_the_date( 'l F j, Y' ); echo $post_date; ?></span>
			</div>
		</div>

	</div>
</div>

<?php do_action( 'accelerate_after_post_content' ); ?>