<?php
// hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_book_taxonomies', 0 );

// create two taxonomies, markets and writers for the post type "book"
function create_book_taxonomies() {
	// Add new taxonomy, make it hierarchical (like categories)
	$labels = array(
		'name'              => _x( 'Markets', 'taxonomy general name', 'textdomain' ),
		'singular_name'     => _x( 'Market', 'taxonomy singular name', 'textdomain' ),
		'search_items'      => __( 'Search Markets', 'textdomain' ),
		'all_items'         => __( 'All Markets', 'textdomain' ),
		'parent_item'       => __( 'Parent Market', 'textdomain' ),
		'parent_item_colon' => __( 'Parent Market:', 'textdomain' ),
		'edit_item'         => __( 'Edit Market', 'textdomain' ),
		'update_item'       => __( 'Update Market', 'textdomain' ),
		'add_new_item'      => __( 'Add New Market', 'textdomain' ),
		'new_item_name'     => __( 'New Market Name', 'textdomain' ),
		'menu_name'         => __( 'Markets', 'textdomain' ),
	);

	$args = array(
		'hierarchical'      => true,
		'labels'            => $labels,
		'show_ui'           => true,
		'show_admin_column' => true,
		'query_var'         => true,
		'rewrite'           => array( 'slug' => 'market' ),
	);

	register_taxonomy( 'market', array( 'cos-project' ), $args );

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Product Categories', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Product Category', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Product Categories', 'textdomain' ),
		'popular_items'              => __( 'Popular Product Categories', 'textdomain' ),
		'all_items'                  => __( 'All Product Categories', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Product Category', 'textdomain' ),
		'update_item'                => __( 'Update Product Category', 'textdomain' ),
		'add_new_item'               => __( 'Add New Product Category', 'textdomain' ),
		'new_item_name'              => __( 'New Product Category Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate product categories with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove product categories', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used product categories', 'textdomain' ),
		'not_found'                  => __( 'No product categories found.', 'textdomain' ),
		'menu_name'                  => __( 'Product Solutions', 'textdomain' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'product-category' ),
	);

	register_taxonomy( 'product-category', 'cos-project', $args );

	// Add new taxonomy, NOT hierarchical (like tags)
	$labels = array(
		'name'                       => _x( 'Locations', 'taxonomy general name', 'textdomain' ),
		'singular_name'              => _x( 'Location', 'taxonomy singular name', 'textdomain' ),
		'search_items'               => __( 'Search Locations', 'textdomain' ),
		'popular_items'              => __( 'Popular Locations', 'textdomain' ),
		'all_items'                  => __( 'All Locations', 'textdomain' ),
		'parent_item'                => null,
		'parent_item_colon'          => null,
		'edit_item'                  => __( 'Edit Location', 'textdomain' ),
		'update_item'                => __( 'Update Location', 'textdomain' ),
		'add_new_item'               => __( 'Add New Location', 'textdomain' ),
		'new_item_name'              => __( 'New Location Name', 'textdomain' ),
		'separate_items_with_commas' => __( 'Separate locations with commas', 'textdomain' ),
		'add_or_remove_items'        => __( 'Add or remove locations', 'textdomain' ),
		'choose_from_most_used'      => __( 'Choose from the most used locations', 'textdomain' ),
		'not_found'                  => __( 'No locations found.', 'textdomain' ),
		'menu_name'                  => __( 'Locations', 'textdomain' ),
	);

	$args = array(
		'hierarchical'          => false,
		'labels'                => $labels,
		'show_ui'               => true,
		'show_admin_column'     => true,
		'update_count_callback' => '_update_post_term_count',
		'query_var'             => true,
		'rewrite'               => array( 'slug' => 'location' ),
	);

	register_taxonomy( 'location', 'cos-manufacturer', $args );

		// Add new taxonomy, NOT hierarchical (like tags)
		$labels = array(
			'name'                       => _x( 'Manufacturers', 'taxonomy general name', 'textdomain' ),
			'singular_name'              => _x( 'Manufacturer', 'taxonomy singular name', 'textdomain' ),
			'search_items'               => __( 'Search Manufacturers', 'textdomain' ),
			'popular_items'              => __( 'Popular Manufacturers', 'textdomain' ),
			'all_items'                  => __( 'All Manufacturers', 'textdomain' ),
			'parent_item'                => null,
			'parent_item_colon'          => null,
			'edit_item'                  => __( 'Edit Manufacturer', 'textdomain' ),
			'update_item'                => __( 'Update Manufacturer', 'textdomain' ),
			'add_new_item'               => __( 'Add New Manufacturer', 'textdomain' ),
			'new_item_name'              => __( 'New Manufacturer Name', 'textdomain' ),
			'separate_items_with_commas' => __( 'Separate manufacturers with commas', 'textdomain' ),
			'add_or_remove_items'        => __( 'Add or remove manufacturers', 'textdomain' ),
			'choose_from_most_used'      => __( 'Choose from the most used manufacturers', 'textdomain' ),
			'not_found'                  => __( 'No manufacturers found.', 'textdomain' ),
			'menu_name'                  => __( 'Manufacturers', 'textdomain' ),
		);

		$args = array(
			'hierarchical'          => false,
			'labels'                => $labels,
			'show_ui'               => true,
			'show_admin_column'     => true,
			'update_count_callback' => '_update_post_term_count',
			'query_var'             => true,
			'rewrite'               => array( 'slug' => 'location' ),
		);

		register_taxonomy( 'manufacturer', 'cos-project', $args );
}
?>