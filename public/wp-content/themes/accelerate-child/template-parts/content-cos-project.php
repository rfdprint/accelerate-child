<div class="row__medium-4">
	<div class="blog-cos wrapper--shadow">



		<div class="project__posts__image">
			<?php
					$allowed_html = cos_allowed_html();
					$page_banner = get_field( 'page_banner_background_image' );
					$image =  wp_kses( cos_get_acf_image( $page_banner, 'cosProject' ), $allowed_html );
					echo $image;


			?>
		</div>
		<div class="headline--small project__posts__title">
			<a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a>
		</div>
		<div class="project__posts__excerpt ">
			<?php

						$content = get_field( 'cos_project_description' );
				if ( $content ) :
						$content = cos_general_content_trim_words( $content );
						echo $content;

			?>
						<!--<span><a class="project__read-more" href="<?php the_permalink(); ?>">Read More</a></span>-->

				<?php endif ?>
		</div>
	</div>
</div>
